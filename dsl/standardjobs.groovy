// Read the contents of the gathered-jobs.json file a step created for us previously
def jobsToParse = readFileFromWorkspace('gathered-jobs.json')
def knownJobs = new groovy.json.JsonSlurper().parseText( jobsToParse )

// Iterate over all of the known jobs and create the necessary platform files
knownJobs.each {
	// Create our job name
	def jobName = "${it.product}/${it.name}/${it.branchGroup} ${it.platform}"

	// Make sure the folders for this exist
	folder( "${it.product}" )
	folder( "${it.product}/${it.name}" )

	// Read in the necessary Pipeline template
	def pipelineTemplate = readFileFromWorkspace("${it.pipelineTemplate}")
	// Now we can construct our Pipeline script
	// We append a series of variables to the top of it to provide a variety of useful information to the otherwise templated script
	// These appended variables are what makes one build different to the next, aside from the template which was used
	def pipelineScript = """
		|def repositoryUrl = "${it.repositoryUrl}"
		|def browserUrl = "${it.browserUrl}"
		|def branchToBuild = "${it.branch}"
		|def productName = "${it.product}"
		|def projectName = "${it.name}"
		|def branchGroup = "${it.branchGroup}"
		|def currentPlatform = "${it.platform}"
		|def ciEnvironment = "${it.environment}"
		|def buildFailureEmails = "${it.buildFailureEmails}"
		|def unstableBuildEmails = "${it.unstableBuildEmails}"

		|${pipelineTemplate}""".stripMargin()

	// Actually create the job now
	pipelineJob( jobName ) {
		properties {
			// We don't want to keep build results forever
			// We'll set it to keep the last 25 builds and discard everything else
			buildDiscarder {
				strategy {
					logRotator {
						numToKeepStr("25")
						daysToKeepStr('')
						artifactDaysToKeepStr('')
						artifactNumToKeepStr('')
					}
				}
			}
			// We don't want to be building the same project more than once
			// This is to prevent one project hogging resources
			// And also has a practical component as otherwise an older build could finish afterwards and upload old build results
			disableConcurrentBuilds()
		}
		triggers {
			// We want to enable SCM Polling so that git.kde.org can tell Jenkins to look for changes
			// At the same time, we don't want Jenkins scanning for changes, so set the Polling specification to be empty so nothing gets scheduled
			pollSCM {
				scmpoll_spec('')
				ignorePostCommitHooks(false)
			}
		}
		// This is where the Pipeline script actually happens :)
		definition {
			cps {
				script( pipelineScript )
				sandbox()
			}
		}
	}
}
